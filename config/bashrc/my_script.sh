#custom aliases
alias snr='service nginx restart'
alias sar='service apache2 restart'
alias bash='nano /root/.bashrc'
alias def='cd /home/vagrant/sites'
alias dplay='def && cd fusion.dev/docs/wp-content/themes/wp-theme-dplay-nl && gss'
alias relog='exit && sudo su'
alias gss='git status'
alias gp='git pull'
alias gcd='git checkout development'
alias gct='git checkout test'
alias gcm='git checkout master'
alias gc='git checkout'
alias gr='git reset'
alias grh='git reset --hard HEAD'
alias ga='git add -A && git commit '
alias pc='composer '
alias pa='php artisan '
alias la='ls -a'
alias gb='git branch'
up () {    # default to current directory if no previous
    local prevdir="./"
    local cwd=${PWD##*/}
    if [[ -z $cwd ]]; then
        # $PWD must be /
        echo 'No previous directory.' >&2
        return 1
    fi
    for x in ../*/; do
        if [[ ${x#../} == ${cwd}/ ]]; then
            # found cwd
            if [[ $prevdir == ./ ]]; then
                echo 'No previous directory.' >&2
                return 1
            fi
            cd "$prevdir"
            return
        fi
        if [[ -d $x ]]; then
            prevdir=$x
        fi
    done
    # Should never get here.
    echo 'Directory not changed.' >&2
    return 1
}

down () {    #
    local foundcwd=
    local cwd=${PWD##*/}
    if [[ -z $cwd ]]; then
        # $PWD must be /
        echo 'No next directory.' >&2
        return 1
    fi
    for x in ../*/; do
        if [[ -n $foundcwd ]]; then
            if [[ -d $x ]]; then
                cd "$x"
                return
            fi
        elif [[ ${x#../} == ${cwd}/ ]]; then
            foundcwd=1
        fi
    done
    echo 'No next directory.' >&2
    return 1
}

masterupdate () {    #
        for x in ../*dni*; do
                echo 'Updating Repo:' $x
                cd $x
                git stash && git reset --hard HEAD && git checkout master && git pull origin master
        done
        echo 'dni directories set to master and updated'
        echo 'stashed changes: ' git stash list
        return 1
}
#Creates new site settings to nginx with provided name and also sets up a directory into /var/www/NAME with index.php file
newsite(){    #
    echo 'Enter site name'
    read site
    echo 'new site:' $site
    touch /etc/nginx/sites-available/$site
    chmod 777 /etc/nginx/sites-available/$site

    echo 'server {'                                           >  /etc/nginx/sites-available/$site
    echo '  root /var/www/'$site';'                           >> /etc/nginx/sites-available/$site
    echo '  index index.php;'                                 >> /etc/nginx/sites-available/$site
    echo '  server_name '$site';'                             >> /etc/nginx/sites-available/$site
    echo '  listen 80;'                                       >> /etc/nginx/sites-available/$site
    echo ' '                                                  >> /etc/nginx/sites-available/$site
    echo '  location / {'                                     >> /etc/nginx/sites-available/$site
    echo '    autoindex on;'                                  >> /etc/nginx/sites-available/$site
    echo '    try_files $uri $uri/ /index.php?q=$uri&$args;'  >> /etc/nginx/sites-available/$site
    echo '  }'                                                >> /etc/nginx/sites-available/$site
    echo ' '                                                  >> /etc/nginx/sites-available/$site
    echo '  error_log /var/log/nginx/'$site'.err;'            >> /etc/nginx/sites-available/$site
    echo '  access_log /var/log/nginx/'$site'.acc;'           >> /etc/nginx/sites-available/$site
    echo ''                                                   >> /etc/nginx/sites-available/$site
    echo '  include /etc/nginx/nginx.conf.fpm;'               >> /etc/nginx/sites-available/$site
    echo '  include /etc/nginx/nginx.conf.sites;'             >> /etc/nginx/sites-available/$site
    echo '}'                          >> /etc/nginx/sites-available/$site

    cp -s /etc/nginx/sites-available/$site /etc/nginx/sites-enabled/$site
    cd /var/www
    mkdir $site
    chmod 775 $site
    cd /var/www/$site
    touch index.php
    echo '<?php phpinfo(); ?>' > index.php
    service nginx restart
    ls
}