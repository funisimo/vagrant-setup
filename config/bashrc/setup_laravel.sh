if dpkg -s php5-mcrypt | grep  Status; then
	cd /var/www/laravel
	echo '---------------------------------------updating composer---------------------------------'
	composer update -q
else
	echo '---------------------------------------setting up laravel---------------------------------'
	apt-get -q=2 install php5-curl php5-gd php5-imagick -y
	apt-get -q=2 install php5-mcrypt php5-mongo -y
	echo "extension=mongo.so" >> /etc/php5/fpm/php.ini
	apt-get -q=2 install mcrypt -y
	sudo php5enmod mcrypt
	sudo service php5-fpm restart
	sudo service nginx restart
	touch /etc/nginx/sites-available/laravel
	chmod 777 /etc/nginx/sites-available/laravel

	echo 'server {'                                           >  /etc/nginx/sites-available/laravel
	echo '  root /var/www/laravel/public;'                    >> /etc/nginx/sites-available/laravel
	echo '  index index.php;'                                 >> /etc/nginx/sites-available/laravel
	echo '  server_name laravel;'                             >> /etc/nginx/sites-available/laravel
	echo '  listen 80;'                                       >> /etc/nginx/sites-available/laravel
	echo ' '                                                  >> /etc/nginx/sites-available/laravel
	echo '  location / {'                                     >> /etc/nginx/sites-available/laravel
	echo '    autoindex on;'                                  >> /etc/nginx/sites-available/laravel
	echo '    try_files $uri $uri/ /index.php?q=$uri&$args;'  >> /etc/nginx/sites-available/laravel
	echo '  }'                                                >> /etc/nginx/sites-available/laravel
	echo ' '                                                  >> /etc/nginx/sites-available/laravel
	echo '  error_log /var/log/nginx/laravel.err;'            >> /etc/nginx/sites-available/laravel
	echo '  access_log /var/log/nginx/laravel.acc;'           >> /etc/nginx/sites-available/laravel
	echo ''                                                   >> /etc/nginx/sites-available/laravel
	echo '  include /etc/nginx/nginx.conf.fpm;'               >> /etc/nginx/sites-available/laravel
	echo '  include /etc/nginx/nginx.conf.sites;'             >> /etc/nginx/sites-available/laravel
	echo '}'                          						  >> /etc/nginx/sites-available/laravel
	#
	cp -s /etc/nginx/sites-available/laravel /etc/nginx/sites-enabled/laravel #
	mkdir /var/www/laravel
	service nginx restart
	cd /var/www/laravel
	git clone https://bitbucket.org/funisimo/laravel-project .
	echo '---------------------------------------updating composer---------------------------------'
	composer update -q
	sudo chmod 777 -R app/storage	
fi