 echo '-------------------------------------------------running dist-upgrade upgrade and update-------------------------------------------------'
 apt-get -q=2 dist-upgrade -y
 apt-get -q=2 upgrade -y
 apt-get -q=2 update -y

if dpkg -s mc | grep  Status; then
	echo '-------------------------------------------------mc already installed-------------------------------------------------'
else
	echo '-------------------------------------------------setting up mc and root scripts-------------------------------------------------'
	apt-get -q=2  install mc curl -y
	cat my_script.sh >> /root/.bashrc
	cat my_script.sh >> .bashrc
fi

if dpkg -s git | grep  Status; then
	echo '-------------------------------------------------git already installed-------------------------------------------------'
else
	echo '-------------------------------------------------setting up git-------------------------------------------------'
	 apt-get -q=2 install -y git
fi

if dpkg -s nginx | grep  Status; then
	echo '-------------------------------------------------nginx and mongodb already installed-------------------------------------------------'
else
	echo '-------------------------------------------------setting up nginx and php5-------------------------------------------------'
	apt-get -q=2 install php5-common php5-cli php5-cgi php5-fpm -y
	apt-get -q=2 install -y nginx
	sudo cp -rf etc/* /etc
	mkdir /var/www
	chmod 775 /var/www
	chgrp -R vagrant /var/www/
	chmod -R g+rw /var/www/
	chmod g+s /var/www/

	echo '-------------------------------------------------updating mongodb-------------------------------------------------'
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
	echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' |  tee /etc/apt/sources.list.d/mongodb.list
	sudo apt-get -q=2 update -y
	sudo apt-get -q=2 install -y mongodb-org
	echo '-------------------------------------------------updating mongodb complete-------------------------------------------------'
	service mongod start
fi
if dpkg -s composer | grep  Status; then
	echo '-------------------------------------------------composer already installed-------------------------------------------------'
	composer self-update
else
	echo '-------------------------------------------------setting up composer-------------------------------------------------'
	apt-get -q=2 install curl -y
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/local/bin/composer
fi
if dpkg -s samba | grep  Status; then
	echo '-------------------------------------------------samba already setup-------------------------------------------------'
else
	echo '-------------------------------------------------setting up samba-------------------------------------------------'
	apt-get -q=2 install samba -y
	cat /home/vagrant/smb.conf >> /etc/samba/smb.conf
	restart smbd
fi

service php5-fpm restart 
service nginx restart
service mongod restart